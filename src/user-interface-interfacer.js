import { Todo } from "./todo";
import { TodoController } from "./todo-controller";

const userInterfaceInterfacer = () => {
  const todoController = TodoController();

  const createNewTodoFromForm = () => {
    const todoId = document.querySelector(".todo-id").value;
    const todoTitle = document.querySelector("#todo-title").value;
    const todoDescription = document.querySelector("#todo-description").value;
    const todoDueDate = document.querySelector("#todo-due-date").value;
    const todoPriority = document.querySelector("#todo-priority").value;
    const todoProject = document.querySelector("#todo-project").value;

    const newTodo = Todo(
      todoId,
      todoTitle,
      todoDescription,
      todoDueDate,
      todoPriority,
      todoProject
    );

    if (todoId != null) {
      localStorage.setItem(todoId, JSON.stringify(newTodo));
    } else {
      return newTodo;
    }
  };

  const createProjectTitles = (todoArea) => {
    const uniqueProjects = todoController.findUniqueProjects();
    uniqueProjects.length <= 0
      ? uniqueProjects.push("Default")
      : uniqueProjects;

    uniqueProjects.forEach((item) => {
      let newHeader = document.createElement("div");
      newHeader.setAttribute("id", `todo-project-${item.replace(/\s/g, "-")}`);
      newHeader.innerHTML = item;
      newHeader.style.display = "grid";
      newHeader.style.gridColumn = "1";

      todoArea.appendChild(newHeader);
    });
  };

  const createProjectDatalist = () => {
    const uniqueProjects = todoController.findUniqueProjects();
    uniqueProjects.length <= 0
      ? uniqueProjects.push("Default")
      : uniqueProjects;

    const datalist = document.querySelector("#projects-datalist");

    uniqueProjects.forEach((project) => {
      let newProject = document.createElement("option");
      newProject.setAttribute("value", project);

      datalist.appendChild(newProject);
    });
  };

  const createAllTodos = (todoArea) => {
    const allTodos = todoController.loadAllTodos();

    allTodos.forEach((item) => {
      let todo = JSON.parse(item[1]);
      let todoProject = document.querySelector(
        `#todo-project-${todo.project.replace(/\s/g, "-")}`
      );

      const newTodo = document.createElement("div");
      newTodo.innerHTML = `<a id="todo-link" href="">${todo.title}</a>`;
      newTodo.setAttribute("id", todo.id);
      newTodo.style.display = "grid";
      newTodo.style.gridColumn = "2";

      if (todoProject !== undefined || todoProject !== null) {
        todoProject.appendChild(newTodo);
      } else {
        let blankHeader = document.createElement("tr");
        blankHeader.appendChild(newTodo);
        todoArea.appendChild(blankHeader);
      }
    });
  };

  const saveTodo = (todo) => {
    todoController.saveTodo(todo);
  };

  const loadTodo = (id) => {
    return todoController.getTodoById(id);
  };

  const deleteTodo = (id) => {
    todoController.deleteTodo(id);
  };

  const displayTodo = (todo) => {
    document.querySelector(".todo-id").value = todo.id;
    document.querySelector("#todo-title").value = todo.title;
    document.querySelector("#todo-description").value = todo.description;
    document.querySelector("#todo-due-date").value = todo.dueDate;
    document.querySelector("#todo-priority").value = todo.priority;
    document.querySelector("#todo-project").value = todo.project;
  };

  return {
    createNewTodoFromForm,
    createProjectTitles,
    createProjectDatalist,
    createAllTodos,
    saveTodo,
    loadTodo,
    deleteTodo,
    displayTodo,
  };
};

export { userInterfaceInterfacer };
