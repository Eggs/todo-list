import { initalLoad } from "./init-load";
import { userInterfaceInterfacer } from "./user-interface-interfacer";

const userInterface = userInterfaceInterfacer();

const content = document.querySelector("#content");

initalLoad(content);

const todoArea = document.querySelector("#todo-area #todo-grid");
//const testTodo = Todo('Another Test', 'PROJECT_1', new Date('2020', '2', '2'), '100', 'Shopping List');
//todoController.saveTodo(testTodo);

userInterface.createProjectTitles(todoArea);
userInterface.createProjectDatalist();
userInterface.createAllTodos(todoArea);

const showTodoForm = [
  document.querySelector("#show-todo-form"),
  document.querySelector(".close"),
];
showTodoForm.forEach((button) => {
  button.addEventListener("click", () => {
    const todoForm = document.querySelector("#new-todo-form");
    if (todoForm.style.display === "none") {
      todoForm.style.display = "grid";
    } else {
      todoForm.style.display = "none";
      todoForm.reset();
    }
  });
});

const todoSubmit = document.querySelector("#todo-submit");
todoSubmit.addEventListener("click", (e) => {
  const todoForm = document.querySelector("#new-todo-form");

  e.preventDefault();
  todoForm.submit();
  const todo = userInterface.createNewTodoFromForm();
  userInterface.saveTodo(todo);
  todoForm.style.display = "none";
  todoForm.reset();
});

const todoDelete = document.querySelector("#todo-delete");
todoDelete.addEventListener("click", () => {
  const todoId = document.querySelector(".todo-id").value;
  userInterface.deleteTodo(todoId);
});

const todos = todoArea.querySelectorAll("#todo-link");
todos.forEach((todo) => {
  todo.addEventListener("click", (e) => {
    e.preventDefault();

    const todoForm = document.querySelector("#new-todo-form");
    const parent = todo.parentElement.getAttribute("id");
    const savedTodo = JSON.parse(userInterface.loadTodo(parent));
    userInterface.displayTodo(savedTodo);
    todoForm.style.display = "grid";
  });
});
