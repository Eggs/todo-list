const initalLoad = (content) => {
    const pageTitle = document.createElement('h1');
    pageTitle.innerHTML = 'ThingsTodo';

    const todoArea = document.createElement('div');
    todoArea.setAttribute('id', 'todo-area');

    // const newTodoField = document.createElement('input');
    // newTodoField.setAttribute('id', 'new-todo-input');
    // newTodoField.setAttribute('placeholder', 'Create a new todo');

    const newTodoTable = document.createElement('div');
    newTodoTable.setAttribute('id', 'todo-grid');

    content.append(
        pageTitle,
        todoArea
    );

    todoArea.append(
        newTodoTable
    );
}

export {
    initalLoad
}