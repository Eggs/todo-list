const TodoController = () => {
  const saveTodo = (todo) => {
    try {
      localStorage.setItem(todo.id, JSON.stringify(todo));
    } catch (error) {
      console.log("There was an error saving to local storage.");
      console.log(`Error: ${error}`);
    }
  };

  const getTodoById = (id) => {
    try {
      return localStorage.getItem(id);
    } catch (error) {
      console.log("There was an error fetching that Todo");
      console.log(error);
    }
  };

  const deleteTodo = (id) => {
    try {
      localStorage.removeItem(id);
    } catch (error) {
      console.log("There was an error fetching that Todo");
      console.log(error);
    }
  };

  const loadAllTodos = () => {
    let allTodos = [];
    try {
      allTodos = Object.entries(localStorage).filter((e) => {
        for (let i of e) {
          return e[0].substring(0, 4) == "todo";
        }
      });
    } catch (error) {
      console.log("There was an error loading from local storage.");
      console.log(`Error: ${error}`);
    }
    return allTodos;
  };

  const findUniqueProjects = () => {
    let uniqueProjects = [];
    loadAllTodos().forEach((item) => {
      let i = JSON.parse(item[1]).project;
      if (!uniqueProjects.some((arrVal) => i == arrVal)) {
        uniqueProjects.push(i);
      }
    });
    return uniqueProjects;
  };

  return {
    findUniqueProjects,
    saveTodo,
    loadAllTodos,
    getTodoById,
    deleteTodo,
  };
};

export { TodoController };
