import { v4 as uuidv4 } from "uuid";

const Todo = (id, title, description, dueDate, priority, project) => {
  if (id == null) {
    return {
      id: `todo-${uuidv4()}`,
      title,
      description,
      dueDate,
      priority,
      project,
    };
  } else {
    return {
      id,
      title,
      description,
      dueDate,
      priority,
      project,
    };
  }
};

export { Todo };
